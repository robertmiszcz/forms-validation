package pl.Oskar.form.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.Oskar.form.model.forms.RegisterForm;

@Controller
public class RegisterController {

    @GetMapping("/register")
    public String registerPost(ModelMap modelMap){
        modelMap.addAttribute("registerForm", new RegisterForm());
        return "register";
    }

    @PostMapping("/register")
    public String registerPost(@ModelAttribute RegisterForm registerForm, ModelMap modelMap){

        modelMap.addAttribute("info","Rejestracja zakończona");
    return "register";
    }









//    @PostMapping("/register")
//
//    public String registerPost(@RequestParam String login,
//                               @RequestParam String password,
//                               @RequestParam String passwordRepeat,
//                               ModelMap modelMap) {
//
//
//      String text = password.equals(passwordRepeat) ? "Zarejestrowano poprawnie" : "Hasła są różne";
//      modelMap.addAttribute("info",text);
//        return "register";
//    }
//
//    @GetMapping("/register")
//    public String registerPost() {
//        return "register";
//    }


}
