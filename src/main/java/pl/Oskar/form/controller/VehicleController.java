package pl.Oskar.form.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.Oskar.form.model.forms.VehicleForm;

import javax.validation.Valid;

@Controller
public class VehicleController {

    @GetMapping("/vehicle")
    public String vehicleGet(ModelMap modelMap){

        modelMap.addAttribute("vehicleForm", new VehicleForm());
            return "vehicle";
    }

    @PostMapping("/vehicle")
    public String vehiclePost(@ModelAttribute @Valid VehicleForm vehicleForm, BindingResult result, ModelMap modelMap){

        if (result.hasErrors()){
            modelMap.addAttribute("info", "Wypełnij poprawnie formularz");
            return "vehicle";
        }
        modelMap.addAttribute("info", vehicleForm.getProductionYear());
        modelMap.addAttribute("infocolor",vehicleForm.getProductionYear()>2000 ? "green": "red");
        modelMap.addAttribute("vehicleForm", new VehicleForm());

        return "vehicle";
    }

}
