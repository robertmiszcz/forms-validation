package pl.Oskar.form.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Random;

@Controller
public class MainController {

    @GetMapping("/index")
    public String index(ModelMap modelMap){

        String[] texts = {"Hej", "Ale jestem głodny" ,"Napiłby sie"};
        Random random = new Random();
        int a = random.nextInt(texts.length);
        modelMap.addAttribute("siema", texts[a]);

        return "index";
    }

    @GetMapping("/message/{age}")
    @ResponseBody
    public String page(@PathVariable int age){

        String text = age>=18 ?  "Jesteś pełnoletni" : "Nie jesteś pełnoletni";
        return text ;
    }

}
