package pl.Oskar.form.model.forms;

import javax.validation.constraints.*;

public class VehicleForm {

    @Min(1900)
    @Max(2017)
    private int productionYear;
    @NotBlank
    @Size(min = 3, max = 30)
    private String mark;
    @NotBlank
    @Size(min = 3, max = 30)
    private String model;

    public VehicleForm() {
    }

    public int getProductionYear() {

        return productionYear;
    }

    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public VehicleForm(int productionYear, String mark, String model) {

        this.productionYear = productionYear;
        this.mark = mark;
        this.model = model;
    }
}
